
/**
 * Problem with this kind of approach is that
 * one has to put and copy a lot of code. As a 
 * result their is ambiguity in the code.
 * 
 * NOT good to proceed with approach	
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CopyEverything {
	public static void main(String[] args) {
		List<Apple> apple = Arrays.asList(new Apple("green", 20), new Apple("green", 50), new Apple("red", 80));
		List<Apple> finalWeightResult = filterWeight(apple);
		List<Apple> finalColorResult = filterColor(apple);
		System.out.println(finalWeightResult);
		System.out.println(finalColorResult);
	}

	public static List<Apple> filterWeight(List<Apple> invetory) {
		List<Apple> result = new ArrayList<>();
		for (Apple apple : invetory) {
			if (apple.getWeight() > 40) {
				result.add(apple);
			}
		}
		return result;
	}

	public static List<Apple> filterColor(List<Apple> invetory) {
		List<Apple> result = new ArrayList<>();
		for (Apple apple : invetory) {
			if ("green".equals(apple.getColor())) {
				result.add(apple);
			}
		}
		return result;
	}

}
