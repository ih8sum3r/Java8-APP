
/**
 * Abstraction over list type can accept any type of stuff
 * All we have to do is to pass "T" in diamond operator.
 * Much clean and dynamic code.
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AbstractionOverList {
	public static void main(String[] args) {
		List<Apple> apple = Arrays.asList(new Apple("green", 20), new Apple("green", 50), new Apple("red", 80));
		List<Apple> getWeight = filter(apple, (Apple app) -> app.getWeight() > 50);
		List<Apple> getColor = filter(apple, (Apple app) -> "green".equals(app.getColor()));

		System.out.println(getWeight);
		System.out.println(getColor);
	}

	// Introduce type parameter T in predicate
	public interface Predicate<T> {
		boolean test(T t);
	}

	public static <T> List<T> filter(List<T> inventory, Predicate<T> p) {
		List<T> result = new ArrayList<>();
		for (T t : inventory) {
			if (p.test(t)) {
				result.add(t);
			}
		}
		return result;
	}
}
