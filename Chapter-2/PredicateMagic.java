
/**
 * Predicate is one of the good approach when it comes to
 * Behaviors parameterization. With the help of strategy design pattern
 * one can accomplish this goal. 
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PredicateMagic {
	public static void main(String[] args) {
		List<Apple> apple = Arrays.asList(new Apple("green", 20), new Apple("green", 50), new Apple("red", 80));
		List<Apple> greenColor = filter(apple, new AppleColorPredicate());
		List<Apple> getWeight = filter(apple, new AppleWeightPredicate());
		List<Apple> greenColorAndHighWeight = filter(apple, new GreenColorAndHighWeight());
		System.out.println(greenColor);
		System.out.println(getWeight);
		System.out.println(greenColorAndHighWeight);
	}

	public static List<Apple> filter(List<Apple> inventory, Predicate p) {
		List<Apple> result = new ArrayList<>();
		for (Apple a : inventory) {
			if (p.test(a)) {
				result.add(a);
			}
		}
		return result;
	}

	// Create Predicate interface
	public interface Predicate {
		boolean test(Apple apple);
	}

	// AppleColorPredicate
	public static class AppleColorPredicate implements Predicate {

		@Override
		public boolean test(Apple apple) {
			return "green".equals(apple.getColor());
		}

	}

	// AppleWeightPredicate
	public static class AppleWeightPredicate implements Predicate {

		@Override
		public boolean test(Apple apple) {
			return apple.getWeight() > 50;
		}

	}

	// Implement complex operations in one
	// green color and weight more than 20
	public static class GreenColorAndHighWeight implements Predicate {

		@Override
		public boolean test(Apple apple) {
			return "green".equals(apple.getColor()) && apple.getWeight() > 20;
		}

	}
}
