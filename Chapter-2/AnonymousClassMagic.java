
/**
 * Anonymous class used to make predicate class more simple and
 * easy to work but anonymous class are kinda difficult to understand at
 * first glance.
 * Anonymous class are one that has no class name and those are instantiate at same time. 
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AnonymousClassMagic {
	public static void main(String[] args) {
		List<Apple> apple = Arrays.asList(new Apple("green", 20), new Apple("green", 50), new Apple("red", 80));
		// Anonymous class
		List<Apple> getColorStuff = filter(apple, new Predicate() {
			public boolean test(Apple apple) {
				return "green".equals(apple.getColor());
			}
		});
		List<Apple> getWeight = filter(apple, new Predicate() {
			public boolean test(Apple apple) {
				return apple.getWeight() > 30;
			}
		});
		System.out.println(getColorStuff);
		System.out.println(getWeight);
	}

	// create predicate
	public interface Predicate {
		boolean test(Apple apple);
	}

	// Filter method
	public static List<Apple> filter(List<Apple> inventory, Predicate p) {
		List<Apple> result = new ArrayList<>();
		for (Apple a : inventory) {
			if (p.test(a)) {
				result.add(a);
			}
		}
		return result;
	}
}
