
/**
 * Somehow good approach not that effective. One can now
 * create only few methods as compared to copying everything
 * everytime
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ParameteringColorAndData {
	public static void main(String[] args) {
		List<Apple> apple = Arrays.asList(new Apple("green", 20), new Apple("green", 50), new Apple("red", 80));
		List<Apple> finalWeightResult = filterWeight(apple, 40);
		List<Apple> finalGreenColorResult = filterColor(apple, "green");
		List<Apple> finalRedColorResult = filterColor(apple, "red");
		System.out.println(finalWeightResult);
		System.out.println(finalGreenColorResult);
		System.out.println(finalRedColorResult);
	}

	public static List<Apple> filterWeight(List<Apple> invetory, int weight) {
		List<Apple> result = new ArrayList<>();
		for (Apple apple : invetory) {
			if (apple.getWeight() > weight) {
				result.add(apple);
			}
		}
		return result;
	}

	public static List<Apple> filterColor(List<Apple> invetory, String color) {
		List<Apple> result = new ArrayList<>();
		for (Apple apple : invetory) {
			if (apple.getColor().equals(color)) {
				result.add(apple);
			}
		}
		return result;
	}

}
