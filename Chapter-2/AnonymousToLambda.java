
/**
 * Anonymous to lambda expression refactoring
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AnonymousToLambda {
	public static void main(String[] args) {
		List<Apple> apple = Arrays.asList(new Apple("green", 20), new Apple("green", 50), new Apple("red", 80));
		// Lambda Expression
		// As (Apple app) has only one object we can write it as app -> app *
		// <some-operation here>
		List<Apple> getColorStuff = filter(apple, app -> "green".equals(app.getColor()));
		List<Apple> getWeight = filter(apple, (Apple app) -> app.getWeight() > 60);
		System.out.println(getColorStuff);
		System.out.println(getWeight);
	}

	// create predicate
	public interface Predicate {
		boolean test(Apple apple);
	}

	// Filter method
	public static List<Apple> filter(List<Apple> inventory, Predicate p) {
		List<Apple> result = new ArrayList<>();
		for (Apple a : inventory) {
			if (p.test(a)) {
				result.add(a);
			}
		}
		return result;
	}
}
